/*!
 * bu-builder
 *
 * BU generator: https://gitlab.com/pdbjapan/tools/bu-builder
 * 
 * By Gert-Jan Bekker
 * License: MIT
 *   See https://gitlab.com/pdbjapan/tools/bu-builder/blob/master/LICENSE
 */

const zlib = await import("zlib");
const fs = await import("fs");
const fsp = fs.promises;

export function isNumber(n) {return ! isNaN(parseFloat(n)) && isFinite(n);}

export async function exists(loc) {
  try {await fsp.access(loc, fs.constants.F_OK);}
  catch (e) {return false;}
  return true;
}

export function gunzip(input, options) {
  const promise = new Promise(function(resolve, reject) {
    zlib.gunzip(input, options, function (error, result) {
      if(!error) resolve(result);
      else reject(Error(error));
    });
  });
  return promise;
}

export function Deferred() {
  var self = this;
  this.promise = new Promise(function(resolve, reject) {
    self.reject = reject
    self.resolve = resolve
  })
}

export async function readlineGZ(loc, todo) {
  var promise = new Deferred();
  
  var inp = fs.createReadStream(loc);
  if (loc.endsWith(".gz")) inp = inp.pipe(zlib.createGunzip());
  inp.on("error", function() {
    promise.reject();
  });
  
  const readline = await import("readline");
  const readInterface = readline.createInterface({
    input: inp,
    console: false
  });
  readInterface.on('line', todo);
  
  readInterface.on('close', function() {
    promise.resolve();
  });
  
  
  return promise.promise;
}

export class streamWriter {
  #buffer = "";
  #buffer_sync = null;
  constructor(fname) {
    this.fname = fname;
    if (fname.endsWith(".gz")) {
      this.fp = zlib.createGzip();
      this.ws = fs.createWriteStream(fname);
      this.fp.pipe(this.ws);
    }
    else {
      this.fp = fs.createWriteStream(fname);
      this.ws = null;
    }
    this.buffer_size = 96*1024;
  }
  
  async flush() {
    if (this.#buffer_sync) {
      await this.#buffer_sync;
      this.#buffer_sync = null;
    }
    if (! this.fp.write(this.#buffer, "utf-8")) this.#buffer_sync = new Promise( (resolve,reject) => this.fp.once("drain", resolve));
    this.#buffer = "";
  }
  
  async write(content) {
    this.#buffer += content;
    if (this.#buffer.length >= this.buffer_size) await this.flush(); // 128MB buffer
  }
  
  async close() {
    await this.flush();
    if (this.#buffer_sync) {await this.#buffer_sync; this.#buffer_sync = null;}
    this.fp.end();
    this.fp = this.ws = null;
  }
}
