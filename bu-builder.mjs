/*!
 * bu-builder
 *
 * BU generator: https://gitlab.com/pdbjapan/tools/bu-builder
 * 
 * By Gert-Jan Bekker
 * License: MIT
 *   See https://gitlab.com/pdbjapan/tools/bu-builder/blob/master/LICENSE
 */

/* Usage:
The tool takes three options:
- `--in`: input file (mmCIF or mmJSON format) or a valid PDBID
- `--out`: output filename, with *.cif for mmCIF, *.json for mmJSON or *.pdb for PDB flatfile. Add a *.gz to enable gzip compression.
- `--bu`: select assembly_id to output (defaults to `1`)

Examples:
`node bu-builder.mjs --in 6pzd.cif --out 6pzd_bu.json`
`node bu-builder.mjs --in 6pzd.json --out 6pzd_bu.cif`
`node bu-builder.mjs --in 6pzd.cif --out 6pzd_bu.pdb`
`node bu-builder.mjs --in 6pzd --out 6pzd_bu.cif.gz`
`node bu-builder.mjs --in 6pzd --out 6pzd_bu.json.gz`
*/

const fs = await import("fs");
const fsp = fs.promises;

const argv = (await import('minimist')).default(process.argv.slice(2));
const glMatrix = (await import("gl-matrix")).default;

const general = await import("./general.mjs");
const cif = await import("./cif.mjs");

let entry = argv.in;
const assembly_id = argv.bu || 1;
const outfn = argv.out;

// from molmil
async function loadFile(fname, options) {  
  if (options.cif) {
    
    const parser = new cif.CIFparser();
    await general.readlineGZ(fname, function(line) {
      parser.parseLine(line);
      if (parser.error) {
        console.error(`Error found in line ${parser.error[1]}:`);
        console.error("  ", parser.error[2]);
        console.log("  ", parser.error[0]);
        parser.error = null;
      }
    });
    
    const nodeFetch = (await import("node-fetch")).default;
    const request = await nodeFetch("https://data.pdbj.org/pdbjplus/dictionaries/mmcif_pdbx.json");
    await cif.loadCIFdic(cif.parseCIFdictionary(await request.json()));
    cif.cleanJSON_withDict(parser.data);
    var data = parser.data;
  }
  else if (options.gz) var data = JSON.parse((await general.gunzip(await fsp.readFile(fname))).toString());
  else if (options.data) var data = options.data;
  else var data = JSON.parse((await fsp.readFile(fname)).toString());
  
  options.dataname = Object.keys(data)[0];
  const soup = {data: Object.values(data)[0], BUmatrices: {}, BUassemblies: {}};

  var pdbx_struct_oper_list = soup.data.pdbx_struct_oper_list;
  if (pdbx_struct_oper_list) {
    var i, length = pdbx_struct_oper_list.type.length, poly;
    var xmode = ! pdbx_struct_oper_list.hasOwnProperty("matrix[1][1]"), mat;
    for (i=0; i<length; i++) {
      mat = glMatrix.mat4.create();
    
      mat[0] = pdbx_struct_oper_list[xmode ? "matrix11" : "matrix[1][1]"][i];
      mat[4] = pdbx_struct_oper_list[xmode ? "matrix12" : "matrix[1][2]"][i];
      mat[8] = pdbx_struct_oper_list[xmode ? "matrix13" : "matrix[1][3]"][i];
      mat[12] = pdbx_struct_oper_list[xmode ? "vector1" : "vector[1]"][i];
    
      mat[1] = pdbx_struct_oper_list[xmode ? "matrix21" : "matrix[2][1]"][i];
      mat[5] = pdbx_struct_oper_list[xmode ? "matrix22" : "matrix[2][2]"][i];
      mat[9] = pdbx_struct_oper_list[xmode ? "matrix23" : "matrix[2][3]"][i];
      mat[13] = pdbx_struct_oper_list[xmode ? "vector2" : "vector[2]"][i];
    
      mat[2] = pdbx_struct_oper_list[xmode ? "matrix31" : "matrix[3][1]"][i];
      mat[6] = pdbx_struct_oper_list[xmode ? "matrix32" : "matrix[3][2]"][i];
      mat[10] = pdbx_struct_oper_list[xmode ? "matrix33" : "matrix[3][3]"][i];
      mat[14] = pdbx_struct_oper_list[xmode ? "vector3" : "vector[3]"][i];
    
      if ( mat[ 0] == 1 && mat[ 1] == 0 && mat[ 2] == 0 && mat[ 3] == 0 && 
           mat[ 4] == 0 && mat[ 5] == 1 && mat[ 6] == 0 && mat[ 7] == 0 &&
           mat[ 8] == 0 && mat[ 9] == 0 && mat[10] == 1 && mat[11] == 0 && 
           mat[12] == 0 && mat[13] == 0 && mat[14] == 0 && mat[15] == 1    ) soup.BUmatrices[pdbx_struct_oper_list.id[i]] = ["identity operation", mat];
      else soup.BUmatrices[pdbx_struct_oper_list.id[i]] = [pdbx_struct_oper_list.type[i], mat];
    }
    
    soup.AisB = (! pdbx_struct_oper_list || ! soup.data.pdbx_struct_assembly || (pdbx_struct_oper_list.id.length == 1 && soup.data.pdbx_struct_assembly.id == 1));
  
    var pdbx_struct_assembly_gen = soup.data.pdbx_struct_assembly_gen, tmp1, tmp2, tmp3=glMatrix.mat4.create(), j, k, mats;
    try {length = pdbx_struct_assembly_gen.assembly_id.length;} catch (e) {length = 0;}
    var xpnd = function(inp) {
      tmp2 = [];
      inp = inp.split(",");
      for (j=0; j<inp.length; j++) {
        if (inp[j].indexOf("-") != -1) {
          inp[j] = inp[j].replace("(", "").replace(")", "").split("-");
          for (k=parseInt(inp[j][0]); k<parseInt(inp[j][1])+1; k++) tmp2.push(k)
        }
        else tmp2.push(inp[j].replace("(", "").replace(")", ""));
      }
      return tmp2;
    }

    for (i=0; i<length; i++) {
      if (! soup.BUassemblies.hasOwnProperty(pdbx_struct_assembly_gen.assembly_id[i])) soup.BUassemblies[pdbx_struct_assembly_gen.assembly_id[i]] = [];
      mats = [];
      if (pdbx_struct_assembly_gen.oper_expression[i].indexOf(")(") != -1) {
        tmp1 = pdbx_struct_assembly_gen.oper_expression[i].split(")(");
        tmp1[0] = xpnd(tmp1[0].substr(1));
        tmp1[1] = xpnd(tmp1[1].substr(0, tmp1[1].length-1));
        // build new matrices
        for (j=0; j<tmp1[0].length; j++) {
          for (k=0; k<tmp1[1].length; k++) {
            poly = tmp1[0][j]+"-"+tmp1[1][k];
            if (! soup.BUmatrices.hasOwnProperty(poly)) {
              glMatrix.mat4.multiply(tmp3, soup.BUmatrices[tmp1[0][j]][1], soup.BUmatrices[tmp1[1][k]][1]);
              if ( tmp3[ 0] == 1 && tmp3[ 1] == 0 && tmp3[ 2] == 0 && tmp3[ 3] == 0 && 
                   tmp3[ 4] == 0 && tmp3[ 5] == 1 && tmp3[ 6] == 0 && tmp3[ 7] == 0 &&
                   tmp3[ 8] == 0 && tmp3[ 9] == 0 && tmp3[10] == 1 && tmp3[11] == 0 && 
                   tmp3[12] == 0 && tmp3[13] == 0 && tmp3[14] == 0 && tmp3[15] == 1    ) soup.BUmatrices[poly] = ["identity operation", tmp3];
              else soup.BUmatrices[poly] = ["combined", glMatrix.mat4.clone(tmp3)];
            }
            mats.push(poly);
          }
        }
      }
      else {
        mats = xpnd(pdbx_struct_assembly_gen.oper_expression[i]);
      }
      soup.BUassemblies[pdbx_struct_assembly_gen.assembly_id[i]].push([mats, pdbx_struct_assembly_gen.asym_id_list[i].split(",")]);
    }
  }

  return BU2JSO(assembly_id, options, soup);
}

// from molmil
function BU2JSO(assembly_id, options, soup) {
  options = options || {};
  
  var atom_site = {group_PDB: [], type_symbol: [], label_atom_id: [], label_alt_id: [], label_comp_id: [], label_asym_id: [], label_entity_id: [], label_seq_id: [], Cartn_x: [], Cartn_y: [], Cartn_z: [], auth_asym_id: [], id: []};
  if (options.modelMode) atom_site.pdbx_PDB_model_num = [];
  const pdbx_chain_remapping = {entity_id: [], label_asym_id: [], auth_asym_id: [], orig_label_asym_id: [], orig_auth_asym_id: [], applied_operations: []};
  
  const pcr_list = {};
  
  const data = soup.data.atom_site;
  
  if (assembly_id == -1) var BU = [[[null], undefined]];
  else var BU = soup.BUassemblies[assembly_id];
  
  var p=0, i, a, asym_ids, id = 1;
  var m, matrix, buid;
  var oldChain, oldResidue, oldAtom;
  var xyzin = glMatrix.vec3.create(), xyzout = glMatrix.vec3.create(), cp, bucounter = 1;
  
  var buid_identity = null;
  for (p in soup.BUmatrices) if (soup.BUmatrices[p][0] == "identity operation") {buid_identity = p; break;}
  
  for (p=0; p<BU.length; p++) {
    asym_ids = BU[p][1] ? new Set(BU[p][1]) : BU[p][1];
    
    for (cp=0; cp<BU[p][0].length; cp++) {
      m = BU[p][0][cp] == null ? ["identity operation", glMatrix.mat4.create()] : soup.BUmatrices[BU[p][0][cp]];
      if (options.modelMode) buid = null;
      if (m[0] == "identity operation") buid = "";
      else {buid = "-"+BU[p][0][cp]; bucounter++;}
      matrix = m[1];
      
      for (a=0; a<data.id.length; a++) {
        if (asym_ids !== undefined && ! asym_ids.has(data.label_asym_id[a])) continue;
        
        const label_asym_id = data.label_asym_id[a]+buid;
        
        if (! (label_asym_id in pcr_list)) pcr_list[label_asym_id] = {
          entity_id: data.label_entity_id[a],
          label_asym_id: data.label_asym_id[a]+buid,
          auth_asym_id: data.auth_asym_id[a]+buid,
          orig_label_asym_id: data.label_asym_id[a],
          orig_auth_asym_id: data.auth_asym_id[a],
          applied_operations: buid == "" ? buid_identity : buid.substr(1)
        };

        xyzin[0] = data.Cartn_x[a];
        xyzin[1] = data.Cartn_y[a];
        xyzin[2] = data.Cartn_z[a];
        glMatrix.vec3.transformMat4(xyzout, xyzin, matrix);
        
        atom_site.group_PDB.push(data.group_PDB[a]);
        atom_site.Cartn_x.push(xyzout[0]);
        atom_site.Cartn_y.push(xyzout[1]);
        atom_site.Cartn_z.push(xyzout[2]);
        atom_site.auth_asym_id.push(data.auth_asym_id[a]+buid);
        atom_site.label_asym_id.push(label_asym_id);
        atom_site.label_alt_id.push(data.label_alt_id[a]);
        atom_site.label_atom_id.push(data.label_atom_id[a]);
        atom_site.label_comp_id.push(data.label_comp_id[a]);
        atom_site.label_entity_id.push(data.label_entity_id[a]);
        atom_site.label_seq_id.push(data.label_seq_id[a]);
        atom_site.type_symbol.push(data.type_symbol[a]);
        atom_site.id.push(id++);

        if (options.modelMode) atom_site.pdbx_PDB_model_num.push(bucounter);
      }
    }
  }
  for (const i of Object.values(pcr_list)) {
    pdbx_chain_remapping.entity_id.push(i.entity_id);
    pdbx_chain_remapping.label_asym_id.push(i.label_asym_id);
    pdbx_chain_remapping.auth_asym_id.push(i.auth_asym_id);
    pdbx_chain_remapping.orig_label_asym_id.push(i.orig_label_asym_id);
    pdbx_chain_remapping.orig_auth_asym_id.push(i.orig_auth_asym_id);
    pdbx_chain_remapping.applied_operations.push(i.applied_operations);
  }
  var obj = {atom_site: atom_site};
  if (! options.modelMode) obj.pdbx_chain_remapping = pdbx_chain_remapping;
  return obj;
}

const options = {};
options.format = options.format || "mmjson";
if (outfn.endsWith(".json.gz") || outfn.endsWith(".json")) options.format = "mmjson";
else if (outfn.endsWith(".cif.gz") || outfn.endsWith(".cif")) options.format = "mmcif";
else if (outfn.endsWith(".pdb.gz") || outfn.endsWith(".pdb")) options.format = "pdb";
else {
  console.error("unknown output format for", outfn);
  process.exit();
}
if (options.format == "pdb") options.modelMode = true;
if (outfn.endsWith(".gz")) options.outgz = true;

if (await general.exists(entry)) {
  if (entry.endsWith(".json")) options.gz = false;
  else if (entry.endsWith(".json.gz")) options.gz = true;
  if (entry.endsWith(".cif")) {options.gz = false; options.cif = true;}
  else if (entry.endsWith(".cif.gz")) {options.gz = true; options.cif = true;}
}
else if (entry.length == 4) {
  const nodeFetch = (await import("node-fetch")).default;
  const request = await nodeFetch(`https://data.pdbj.org/pdbjplus/data/pdb/mmjson/${entry.toLowerCase()}.json`);
  options.data = await request.json();
  entry = null;
}

const bustruct = await loadFile(entry, options);

const fp = new general.streamWriter(outfn);

var out = "";
if (options.format == "mmjson") {
  if (bustruct.atom_site.id.length > 100000) await cif.streamMMJSON(options.dataname+"_BU", bustruct, fp); // more than 100K atoms -> use the stream method
  else {
    var jso_out = {}; jso_out[options.dataname+"_BU"] = bustruct;
    await fp.write(JSON.stringify(jso_out));
  }
}
else if (options.format == "mmcif") {
  var jso_out = {}; jso_out[options.dataname+"_BU"] = bustruct;
  await cif.streamCIF(jso_out, null, fp);
}
else if (options.format == "pdb") {
  var aname, cname, rid, prevMdl;
  for (var aid=0; aid<bustruct.atom_site.Cartn_x.length; aid++) {
    if (prevMdl != bustruct.atom_site.pdbx_PDB_model_num[aid]) {
      if (aid != 0) await fp.write("ENDMDL\n");
      await fp.write("MODEL "+bustruct.atom_site.pdbx_PDB_model_num[aid]+"\n");
      prevMdl = bustruct.atom_site.pdbx_PDB_model_num[aid];
    }
    aname = bustruct.atom_site.label_atom_id[aid].substr(0,4);
    if (! general.isNumber(aname[0]) && aname.length < 4) aname = ' ' + aname;
    cname = (bustruct.atom_site.auth_asym_id[aid] || bustruct.atom_site.label_asym_id[aid] || "").substr(0,2);
    rid = ((bustruct.atom_site.label_seq_id[aid]||"")+"").substr(0,4);

    await fp.write(bustruct.atom_site.group_PDB[aid].padEnd(6) + ((aid+1)+'').padStart(5) + " " + aname.padEnd(4) + " " + bustruct.atom_site.label_comp_id[aid].padStart(3) + cname.padStart(2) + (rid+'').padStart(4) + "    " + bustruct.atom_site.Cartn_x[aid].toFixed(3).padStart(8) + bustruct.atom_site.Cartn_y[aid].toFixed(3).padStart(8) + bustruct.atom_site.Cartn_z[aid].toFixed(3).padStart(8) + "\n");
  }
}

await fp.close();
