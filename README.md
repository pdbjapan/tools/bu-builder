### About

This tool can generate biological units for PDB entries in mmCIF, mmJSON and PDB flatfile format on the server.

### Setup

This app requires nodejs (latest LTS (v16 is required) from https://nodejs.org/en/download/ or via your OS' package manager). 

Then, obtain molmil-app from the repository:  
`git clone https://gitlab.com/pdbjapan/tools/bu-builder.git`
`cd bu-builder`

And install the dependencies (via nodejs' package manager):  
`npm install`

### Usage
The tool takes three options:
- `--in`: input file (mmCIF or mmJSON format) or a valid PDBID
- `--out`: output filename, with *.cif for mmCIF, *.json for mmJSON or *.pdb for PDB flatfile. Add a *.gz to enable gzip compression.
- `--bu`: select assembly_id to output (defaults to `1`)

Examples:
`node bu-builder.mjs --in 6pzd.cif --out 6pzd_bu.json`
`node bu-builder.mjs --in 6pzd.json --out 6pzd_bu.cif`
`node bu-builder.mjs --in 6pzd.cif --out 6pzd_bu.pdb`
`node bu-builder.mjs --in 6pzd --out 6pzd_bu.cif.gz`

For larger entries, the `--max-old-space-size=8192` flag to nodejs might be required (due to high memory usage), e.g.:
`node --max-old-space-size=8192 bu-builder.mjs --in 1m4x.json --out 1m4x_bu.cif.gz`
